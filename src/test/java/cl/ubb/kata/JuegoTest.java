package cl.ubb.kata;

import static org.junit.Assert.*;

import org.junit.Test;

public class JuegoTest {
	
	private String var;
	@Test
	public void JuegoRecibeUnoDevuelveUno() {
	
		
		Juego game=new Juego();
		
		var=game.jugar("1");
		
		assertEquals("1",var);
				
	}
	
	@Test
	public void JuegoRecibeDosDevuelveDos() {
	
		
		Juego game=new Juego();
		
		var=game.jugar("2"); 
		
		assertEquals("2",var);
				
	}
	
	@Test
	public void JuegoRecibeTresDevuelveFizz() {
	
		
		Juego game=new Juego();
		
		var=game.jugar("3"); 
		
		assertEquals("Fizz",var);
				
	}
	
	@Test
	public void JuegoRecibeCuatroDevuelveCuatro() {
	
		
		Juego game=new Juego();
		
		var=game.jugar("4"); 
		
		assertEquals("4",var);
				
	}
	
	@Test
	public void JuegoRecibeCincoDevuelveBuzz() {
	
		
		Juego game=new Juego();
		
		var=game.jugar("5"); 
		
		assertEquals("Buzz",var);
				
	}
	
	@Test
	public void JuegoRecibe15DevuelveFizzBuzz() {
	
		
		Juego game=new Juego();
		
		var=game.jugar("15"); 
		
		assertEquals("FizzBuzz",var);
				
	}
	
	

}
